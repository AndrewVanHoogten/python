number_of_host_addresses = 500
number_of_host_bits = 1
available_host_addresses = 0

while available_host_addresses < number_of_host_addresses and number_of_host_bits < 33:
    number_of_host_bits += 1
    available_host_addresses = (available_host_addresses + 2) * 2 - 2

if number_of_host_bits >= 33:
    print("Dit is onmogelijk in een IPv4 adres!")
else:
    print("Je hebt " + str(number_of_host_bits) + " bits nodig") 
