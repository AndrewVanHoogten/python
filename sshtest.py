import subprocess

#hosts_and_ports.txt gaan uitlezen + lege array maken
IpAdressen = []
PoortApart = []
with open("hosts_and_ports.txt") as hap_fh:

#Voor elke lijn in hosts_and_ports.txt -> steek die in hap_inhoud en append die vervolgens in mijn lege array
    for line in hap_fh.readlines():
        hap_inhoud = line.strip()
        IpAdressen.append(hap_inhoud)

#Split array IpAdressen op zodat het getal na de : alleen komt te staan. Steek dit vervolgens in lege array poortapart.
for Poortnr in IpAdressen:
    PoortApart.append(Poortnr.split(":"))

#Poortnummer apart zetten uit poortapart en steek deze in variabele port
for Portnr in PoortApart:
    Port = str(Portnr[1])
   
#Voor elke poort in hosts_and_ports.txt -> voer subprocess uit en zet de bestanden van commands.txt
#op mijn machines    
    with open ("commands.txt") as fh:
        completed = subprocess.run("ssh ubuntussh@127.0.0.1 -p" + Port, capture_output=True, text=True, shell=True, stdin=fh)
        exitcode  = completed.returncode
        output = completed.stdout
        fout = completed.stderr
        print(exitcode)
        print(output)
        print(fout)