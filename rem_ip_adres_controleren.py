def bestaat_uit_bytes(IPaddress):
    from python_zelf_functies_schrijven import is_byte
    SplitIPAdress = IPaddress.split(".")
    IsIP = None
    for number in SplitIPAdress:
        output = is_byte(int(number))
        if output == False:
            IsIP = False
            break
        else:
            IsIP = True 
    return IsIP      